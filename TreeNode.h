#include<stdio.h>
#include<stdlib.h>


/* Define the data type */
typedef unsigned int  value_t;

/* Data structures used to define the tree. */
typedef struct DataNode {
	value_t value;
} DataNode;


//definicao do tipo do no
typedef struct TreeNode {
	struct TreeNode *left_tree;
	struct TreeNode *mid_tree;
	struct TreeNode *right_tree;
	struct DataNode *left_data;
	struct DataNode *right_data;
} TreeNode;


//prototipo das funcoes da arvore binaria
TreeNode *criar_ArvTri();
TreeNode* insere_ArvTri(TreeNode *raiz, value_t dado_1, value_t dado_2);
value_t *flatten(TreeNode *n, size_t *num_elements);
size_t store_ArvTri(TreeNode *raiz, value_t *dado);
void quicksort(value_t *arr, int first, int last);
void first(value_t *dado, size_t n);
