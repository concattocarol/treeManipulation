#include "TreeNode.h"


//save space for a pointer to a Tree
TreeNode* criar_ArvTri(){
    
    TreeNode* raiz=  (TreeNode*)malloc(sizeof(TreeNode));
	if(raiz == NULL){
		exit(1);
	}
	raiz=NULL;
	return raiz;
}

//Insert data on the Tree
TreeNode * insere_ArvTri(TreeNode *raiz, value_t dado_1, value_t dado_2){
	if(raiz==NULL){
		struct TreeNode *no= (struct TreeNode *) malloc (sizeof(struct TreeNode));
		if(no!=NULL){
			no->left_data= malloc (sizeof(value_t));
			no->left_data->value=dado_1;
	    	no->right_data= malloc (sizeof(value_t));
			no->right_data->value=dado_2;
			no->left_tree=NULL;
			no->mid_tree=NULL;
			no->right_tree=NULL;
			raiz=no;
			printf("\naqui");
		}
	
	}
	else{
		if(dado_1<(raiz->left_data->value)){
			printf("\nleft");
		 	raiz->left_tree=insere_ArvTri(raiz->left_tree,dado_1,dado_2);
	    }
		else
		   if(dado_2<(raiz->right_data->value)){
		   	    printf("\nmid");
		 		raiz->mid_tree=insere_ArvTri(raiz->mid_tree,dado_1,dado_2);
		  	}
		    else{
		    	printf("\nright");
		    	raiz->right_tree=insere_ArvTri(raiz->right_tree,dado_1,dado_2);
		    }
	}
		return raiz;
 }

value_t *flatten(TreeNode *n, size_t *num_elements){
	value_t dado[100];   //vector to store data from Tree
	int i;
	for(i=0;i<100;i++) dado[i]=0;   //clearing vector
	*num_elements= store_ArvTri(n,dado); 
     //quicksort(dado, 0, *num_elements);
     first(dado, *num_elements);
	return dado;
}

// read the data from Tree and return it in dado
size_t store_ArvTri(TreeNode *raiz, value_t *dado){
	if(raiz==NULL) return 0;
	printf("store_ArvTri\n");
    store_ArvTri(raiz->left_tree,dado);
	store_ArvTri(raiz->mid_tree,dado);
	store_ArvTri(raiz->right_tree,dado);
	size_t i=0;  //number of data
	while(dado[i]>0){ //search for last data
		i++;
	}
	if(raiz->left_data!=NULL)  dado[i]=raiz->left_data->value;
	if(raiz->right_data!=NULL) dado[i+1]=raiz->right_data->value;
	return i+1;
}

//put the firts data in the beginnig of the vector
void first(value_t *dado, size_t n){
	value_t aux;
	int i;
	for(i=0;i<=n/2;i++){
		aux=dado[i];
		dado[i]=dado[n-i];
		dado[n-i]=aux;
	}
}

//order any vector using qicksort algorithm
void quicksort(value_t *arr, int first, int last){
 int i,j,pivot,tmp;
 if(first<last)
 {
   pivot=first;
   i=first;
   j=last;
   while(i<j) //order the vector until first does not reach last
   {
     while(arr[i]<=arr[pivot] && i<last)
        i++;
     while(arr[j]>arr[pivot])
        j--;
     if(i<j)
     {
        tmp=arr[i];
        arr[i]=arr[j];
        arr[j]=tmp;
     }
   }
   //order the numbers
   tmp=arr[pivot];
   arr[pivot]=arr[j];
   arr[j]=tmp;
   //next pivot ordering
   quicksort(arr,first,j-1); 
   quicksort(arr,j+1,last);
 }
}




